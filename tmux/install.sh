#!/bin/bash

# Check tmux installed
if ! command -v tmux &>/dev/null; then
	echo "tmux could not be found"
	exit
fi

# Copy tmuxconf
cp .tmux.conf ~/.tmux.conf

# Reset tmux
tmux kill-server
