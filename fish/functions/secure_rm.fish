# Defined in /home/ivaldes/.config/fish/functions/secure_rm.fish @ line 1
function secure_rm 
	while true
    set_color FF0
    echo "Do you want to PERMANENT remove? [y/N]"
    read -l -p "" confirm

        switch $confirm
        case Y y
            if /bin/rm $argv
                echo "Done!"
            end
            return
        case '' N n
            echo "Cancel"
            return 
        end
    end
end
