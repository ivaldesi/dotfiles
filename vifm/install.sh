#!/bin/bash

# Check vifm installed
if ! command -v vifm &>/dev/null; then
	echo "vifm could not be found"
	exit
fi

# Copy vifm config
cp ./vifmrc ~/.config/vifm/
cp -r ./colors ~/.config/vifm/
