#!/bin/bash

# Check code installed
if ! command -v code &>/dev/null; then
	echo "code could not be found"
	exit
fi

# Copy settings
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Copy keybindings and settings
cp keybindings.json ~/.config/Code/User
cp settings.json ~/.config/Code/User

# Install all the extensions
./extensions.sh
