#!/bin/bash

code --install-extension awarest.awarest-align
code --install-extension azemoh.one-monokai
code --install-extension donjayamanne.githistory
code --install-extension eamodio.gitlens
code --install-extension esbenp.prettier-vscode
code --install-extension foxundermoon.shell-format
code --install-extension jeff-hykin.better-dockerfile-syntax
code --install-extension joshuapoehls.json-escaper
code --install-extension lokalise.i18n-ally
code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension ms-toolsai.jupyter
code --install-extension ms-toolsai.jupyter-keymap
code --install-extension SlySherZ.comment-box
code --install-extension stkb.rewrap
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension streetsidesoftware.code-spell-checker-spanish
code --install-extension tuxtina.json2yaml
code --install-extension twxs.cmake
code --install-extension vscodevim.vim
code --install-extension wmaurer.change-case
code --install-extension yzhang.markdown-all-in-one
