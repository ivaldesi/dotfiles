#!/bin/bash

# Check vim installed
if ! command -v vim &>/dev/null; then
	echo "vim could not be found"
	exit
fi

# Install Plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Copy vimrc
cp .vimrc ~/.vimrc

# Install all the plugins
vim +PlugInstall +qall
